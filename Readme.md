<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://netflix.github.io/dgs/images/logo--blue.svg" width="320" alt="Nest Logo" /></a>
  <a href="http://garphql.com/" target="blank"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS973jy0dupz7oRd7Ht9Lp5YHBQ6h2C7No9VA&usqp=CAU" width="100" alt="Graphql Logo" /></a>
</p>


  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Java</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">


## Description

[DGS --> ByNetflix](https://github.com/netflix/dgs-framework/) framework Java | Kotlin repository.
## Dependency in gradle
```groovy
    repositories {
        mavenCentral()
    }
    
    dependencies {
        implementation "com.netflix.graphql.dgs:graphql-dgs-spring-boot-starter:latest.release"
    }
```

## Dependency in Maven
```xml
    <dependency>
            <groupId>com.netflix.graphql.dgs</groupId>
            <artifactId>graphql-dgs-spring-boot-starter</artifactId>
            <!-- Make sure to set the latest framework version! -->
            <version>${dgs.framework.version}</version>
    </dependency>
```

## Stay in touch

- Author - [Sagar Thakkar](mailto:sagar.thakkar@neosoftmail.com)
- Twitter - [@thakkarsagar12](https://twitter.com/thakkarsagar12)


