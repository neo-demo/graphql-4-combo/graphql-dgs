package com.graphql.graphqldgs.services;

import com.graphql.graphqldgs.model.Subject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class SubjectDataService {
    private final List<Subject> subjectList = new ArrayList<>(
        Arrays.asList(Subject.builder().id(1L).title("MATHS").build(),
                Subject.builder().id(2L).title("ENGLISH").build(),
                Subject.builder().id(3L).title("HINDI").build(),
                Subject.builder().id(4L).title("HISTORY").build())
    );



    public List<Subject> findAll()  {
        return this.subjectList;
    }

    public List<Subject> create(Subject subject)  {
        subject.setId((long) this.subjectList.size() + 1);
        this.subjectList.add(subject);
        return this.subjectList;
    }


}
