package com.graphql.graphqldgs.controllers;

import com.graphql.graphqldgs.model.Subject;
import com.graphql.graphqldgs.services.SubjectDataService;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
public class SubjectGraphController {

    @Autowired
    private SubjectDataService subjectDataService;

    @DgsData(parentType = "Query", field = "getAllSubjects")
    public List<Subject> getAllSubjects() {
        return this.subjectDataService.findAll();
    }
    @DgsData(parentType = "Mutation", field = "addSubject")
    public List<Subject> addSubject(@InputArgument("subject") String subject) {
        return this.subjectDataService.create(Subject.builder().title(subject).build());
    }

}
